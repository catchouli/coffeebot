{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module DuckSay (duckSayPlugin) where

import Lambdabot.Plugin
import Lambdabot.Util

import Data.List.Split (chunksOf)
import Text.ParserCombinators.Parsec


type Coffee = ModuleT () LB


-- A Duck is a function which takes some lines
-- and outputs a duck saying them
type Duck = [String] -> [String]


-- A ducksay is some text for a duck to say multiple nested
data DuckSay = DuckSay String | RecursiveDuckSay DuckSay deriving Show


-- Our ducks
duck :: [Duck]
duck =
  [ \txt -> speechBubble 6 txt ++
          [ "           ,~~.                         "
          , "          (  6 )-_,                     "
          , "     (\\___ )=='-'                      "
          , "      \\ .   ) )\"                       "
          , "  ~'`~'\\ `-' /~'`~'`~'`~'`~'`~'`~'`~'`~"
          ]

  {-, \txt -> speechBubble 6 txt ++
          [ "                 ⌒"
          , "            ___/ • )"
          , "          '---,  /"
          , "              ▷.◁____/\")"
          , "              \\   \\)  /"
          , "               \\_____/"
          , "                 _|_|"
          ] -} -- one day i will fix you buddy

  , \txt -> speechBubble 6 txt ++
    [ "       _          _          _          _          _       "
    , "     >(')____,  >(')____,  >(')____,  >(')____,  >(') ___, "
    , "       (` =~~/    (` =~~/    (` =~~/    (` =~~/    (` =~~/ "
    , "~^~^`---'~^~^~^`---'~^~^~^`---'~^~^~^`---'~^~^~^`---'~^~^~ "
    ]

  , \txt -> speechBubble 6 txt ++
    [ "         ,----,"
    , "    ___.`      `,"
    , "    `===  D     :"
    , "      `'.      .'"
    , "         )    (                   ,"
    , "        /      \\_________________/|"
    , "       /                          |"
    , "      |                           ;"
    , "      |               _____       /"
    , "      |      \\       ______7    ,'"
    , "      |       \\    ______7     /"
    , "       \\       `-,____7      ,'"
    , " ^~^~^~^`\\                  /~^~^~^~^"
    , "   ~^~^~^ `----------------' ~^~^~^"
    ]

  , \txt -> speechBubble 6 txt ++
    [ "                         __...           "
    , "                      ,-'     `'.        "
    , "                     .--.   _    `.      "
    , "                    //O / ,' |    `.     "
    , "          .-.       ||_/ / O /     |     "
    , "          \\  `-.._,' __  `\\ /      |   "
    , "           `.          \\   '       |    "
    , "             `.         `.         /     "
    , "               `-.__      '-      /      "
    , "      .-.           '-=...--   _,'       "
    , "      |  \\          ,'        /         "
    , "      |,  \\        /         /          "
    , "      | \\  `.    ,'         /           "
    , "      |  .   `-..'        /'             "
    , "      |        ,'         ''`-.       _  "
    , "      |\\       /               `-._ ,',/"
    ]
  ]


-- Our plugin definition
duckSayPlugin :: Module ()
duckSayPlugin = newModule
  { moduleCmds = return
      [ (command "coffee")
          { help = say "COVFEFE"
          , process = \_ -> processDuckSay "coffee's ready"
          }
      , (command "nocoffee")
          { help = say "COVFEFE"
          , process = \_ -> processDuckSay "there's no fucking coffee"
          }
      , (command "duckSay")
          { help = say "COVFEFE"
          , process = processDuckSay
          }
      ]
  }


-- Outputs a speech bubble with the given text
speechBubble :: Int -> [String] -> [String]
speechBubble offset lines =
                          let textLen = foldr max 0 . map length $ lines
                              spacing = replicate offset ' '
                              pad len txt = take len $ txt ++ repeat ' '
                          in [ " __" ++ replicate textLen '_']
                             ++ map (\txt -> "| " ++ pad textLen txt ++ " |") lines
                             ++ ["|__" ++ replicate textLen '_' ++ "|"]
                             ++ [spacing ++ "\\ \\"]
                             ++ [spacing ++ " \\\\"]
                             ++ [spacing ++ "  \\"]


-- A parser which parses lines like @ducksay @ducksay @ducksay test
-- into either a plain DuckSay or a RecursiveDuckSay
--
-- e.g. parseDuckSay "hello" = DuckSay "hello"
--      parseDuckSay "@ducksay hello" = RecursiveDuckSay (DuckSay "hello")
parseDuckSay :: GenParser Char st DuckSay
parseDuckSay = do
  recursiveDucks <- many (string "@ducksay " >> skipMany (char ' ') >> return RecursiveDuckSay)
  baseDuck <- getInput >>= return . DuckSay
  return $ foldr (.) id recursiveDucks baseDuck


-- Get a random duck and have it say some lines
duckSay :: [String] -> Cmd Coffee [String]
duckSay lines = do
  nextDuck <- random duck
  return $ nextDuck lines


-- Evaluate a DuckSay and convert it to random ducks saying the lines
runDuckSay :: DuckSay -> Cmd Coffee [String]
runDuckSay (DuckSay txt) = duckSay (chunksOf 50 txt)
runDuckSay (RecursiveDuckSay child) = runDuckSay child >>= duckSay


-- Parse and process a duckSay command
processDuckSay :: String -> Cmd Coffee ()
processDuckSay txt = do
  let parseResult = parse parseDuckSay "" txt
  duckResult <- either (const . return $ []) (runDuckSay) parseResult
  mapM_ say duckResult

